# PHP 5

## Requirement

This project requires PHP 5.

## How to run

```bash
php hello-world.php
```

This will display a hello world message if executed with PHP 5 and an error message otherwise.

### One-liner to run the project

```bash
docker build -t php5-app .
docker run php5-app
```
