# PHP 8
## Requirement
This project requires PHP 8.

## How to run
```bash
php hello-world.php
```
This will display a hello world message if executed with PHP 8 and an error message otherwise.


### One-liner to run the project
```bash
docker build -t php-8-app .
docker run php-8-app