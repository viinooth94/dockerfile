# Java 7
## Requirement
This project requires Java 7.

## How to run
```bash
javac HelloWorld.java
java HelloWorld
```
This will display a hello world message if executed with Java 7 and an error message otherwise.

### One-liner to run the project
```bash
docker build -t java-7 .
docker run java-7