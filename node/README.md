# Node
## Requirement
This project requires Node.

No specific version required, try to use a small node image.

## How to run
```bash
node hello-world.js
```
This will display a hello world message.

### One-liner to run the project
```bash
docker build -t node .
docker run node
